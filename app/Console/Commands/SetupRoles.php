<?php

namespace App\Console\Commands;

use App\Helpers\Utils;
use Illuminate\Console\Command;

class SetupRoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup:roles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets up the required default application roles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Utils $utils)
    {
        parent::__construct();
        $this->utils = $utils;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->utils->setupRoles($this);
    }
}
