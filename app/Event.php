<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Event extends Model
{

    use SoftDeletes;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'date', 'time', 'location', 'type', 'name',
        'twitter_hashtag', 'facebook_hashtag','promo_image_url',
        'youtube_live_stream_url','price','download_link'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    private $create_rules = [
        'name' => 'required|string',
        'location' => 'required|string',
        'type' => 'required|string|in:free,paid',
        'price' => 'numeric|required_if:type,paid',
        'date' => 'required|date',
        'time' => 'required',
        'description' => 'required|string',
        'promo_image' => 'string',
        'twitter_hashtag' => 'string',
        'facebook_hashtag' => 'string',
        'youtube_live_stream_url' => 'string',
        'download_link' => 'string',
    ];

    private $errors;

    /**
     * Validator for Event model
     *
     * @param $data
     * @param $scenario
     * @param null $rules
     * @return bool
     */
    public function validate($data,$scenario,$rules=null)
    {
        if(!$rules) {

            switch ($scenario) {
                case $this->SCENARIO_CREATE:
                case $this->SCENARIO_UPDATE:
                    $rules = $this->create_rules;
                    break;
            }
        }

        // make a new validator object
        $v = Validator::make($data, $rules);

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            $this->errors = $v->errors();
            return false;
        }

        // validation pass
        return true;
    }

    /**
     * Get validator error
     * @return mixed
     */
    public function errors()
    {
        return $this->errors;
    }

    /**
     * Get all of the event's likes.
     */
    public function likes()
    {
        return $this->morphMany('App\Like', 'likeable');
    }
}
