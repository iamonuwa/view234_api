<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 1/2/17
 * Time: 10:54 AM
 */

namespace App\Helpers;


use App\User;
use GuzzleHttp\Client;
use Tymon\JWTAuth\Facades\JWTAuth;

class Auth
{
    public static function login($credentials) {
        try {
            if (! $token = JWTAuth::attempt($credentials) )
            {
                return self::_234Login($credentials);
            }
        } catch (JWTException $e) {
            return Response::internalServerError('Internal error');
        }

        $user = User::where(["email" => $credentials['email'], "active" => true])->with('roles')->first();

        if($user) {
            return Response::success(compact("user", "token"), 'Login Successful');
        } else {
            return Response::unauthorized('You are not allowed to login');
        }
    }

    private static function _234Login($credentials) {
        try {
            $client = new Client();
            $res = $client->request('GET', env('_234_API_URL'), [
                'query' => [
                    'username' => $credentials['email'],
                    "password" => $credentials['password'],
                    "insecure" => "cool"
                ]
            ]);

            $body = json_decode( (string) $res->getBody() );

            if($body->status == "ok") {

                User::create([
                    'name' => $body->user->username,
                    'email' => $body->user->email,
                    'password' => bcrypt($credentials["password"]),
                ]);

                return self::login($credentials);

            } else {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            return Response::unauthorized('Wrong email password combination');
        }
    }
}