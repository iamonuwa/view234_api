<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 26/01/17
 * Time: 11:33 PM
 */

namespace App\Helpers;


use DCN\RBAC\Models\Permission;
use DCN\RBAC\Models\Role;

class Utils
{
    const APP_DEFAULT_ROLES = [
        'user' => 'Default role for application users',
        'admin' => 'Admin user with higher privileges'
    ];
    const APP_DEFAULT_PERMISSIONS = [
        'crud.users' => 'Permission to Create, Read, Update, and Delete users',
    ];

    /**
     * Creates default application roles and permissions
     * @param $commander
     */
    public function setupRoles($commander) {
        $commander->info('Checking if all default roles have been setup');

        $all_roles = Role::all()->pluck('slug')->toArray();

        if( count(array_intersect($all_roles, array_keys(self::APP_DEFAULT_ROLES)) ) < count(self::APP_DEFAULT_ROLES) ) {

            $commander->info('Setting up application roles');

            foreach( self::APP_DEFAULT_ROLES as $role => $description) {
                Role::create([
                    'name' => ucfirst($role),
                    'slug' => $role,
                    'description' => $description
                ]);
            }

            $commander->info('Setting up permissions');
            foreach( self::APP_DEFAULT_PERMISSIONS as $perm => $description) {
                Permission::create([
                    'name' => ucfirst($perm),
                    'slug' => $perm,
                    'description' => $description
                ]);
            }

            $commander->info('Assigning Permissions to Roles');
            $adminRole = Role::where("slug", 'admin')->first();
            $crudUsersPermission = Permission::where("slug", 'crud.users')->first();
            $adminRole->attachPermission($crudUsersPermission);

            $commander->info('Roles Setup Completed!');


        } else {
            $commander->error('Application roles are already set up');
        }
    }
}