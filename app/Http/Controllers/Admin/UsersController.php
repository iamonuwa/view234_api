<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Response;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource. Requires Auth
     *
     * @param string $token
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::paginate($request->query('per-page'));
        return Response::success($users);
    }

    /**
     * Not Yet Implemented
     */
    public function create()
    {
        //
    }

    /**
     * Not Yet Implemented
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource. Requires Auth
     *
     * @param  string  $token
     *
     * @param  int  $id
     * ID if the user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findorfail($id)->first();
        if($user) {
            return Response::success($user);
        }
        return Response::notFound();
    }

    /**
     * Not yet implemented
     */
    public function edit($id)
    {
        //
    }

    /**
     * Not yet implemented
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Not Yet implelemented
     */
    public function destroy($id)
    {
       //
    }

    /**
     * Disable User - Requires Auth
     *
     * @param  string  $token
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function disable($id)
    {
        $user =  User::findorFail($id);
        $user->active = false;
        $user->save();

        return Response::success($user);
    }

    /**
     * Enable User - Requires Auth
     *
     * @param  string  $token
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function enable($id)
    {
        $user =  User::findorFail($id);
        $user->active = true;
        $user->save();

        return Response::success($user);
    }
}
