<?php

namespace App\Http\Controllers;

use App\Event;
use App\Helpers\Response;
use App\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    /**
     * Display a paginated list of events.
     *
     * @param string $per-page Number of items returned at once
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $events = Event::paginate($request->query('per-page'));
        return Response::success($events);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified event.
     *
     * @param  int $event ID of the event
     * @return \Illuminate\Http\Response
     */
    public function show($event)
    {
        $event = Event::findorfail($event)->with('likes')->first();
        if($event) {
            $event->views += 1;
            $event->save();

            return Response::success($event);
        }
        return Response::notFound();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Allow user like or unlike an event
     *
     * @param  int $event ID of the event
     * @param  string  $token
     * @return \Illuminate\Http\JsonResponse
     */
    public function like($event, Request $request)
    {
        if( $event = Event::find($event) ) {
            if(in_array(Auth::User()->id, $event->likes->pluck('user_id')->toArray())) {
                Like::where('likeable_type', 'App\Event')
                    ->where('likeable_id', $event->id)
                    ->where('user_id', Auth::User()->id)
                    ->delete();
                return Response::success("Event disliked");

            } else {
                $event->likes()->create([
                    'user_id' => Auth::User()->id,
                ]);
                return Response::success("Event Liked");
            }
        }
        return Response::notFound("Event not found");
    }
}
