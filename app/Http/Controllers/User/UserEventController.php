<?php

namespace App\Http\Controllers\User;

use App\Event;
use App\Helpers\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserEventController extends Controller
{
    /**
     * Display a listing of the resource. Requires Auth
     *
     * @param string $token
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $events = Event::where('user_id', '=', Auth::user()->id)->paginate($request->query('per-page'));
        return Response::success($events);
    }

    /**
     * NOT IMPLEMENTED.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage. Requires Auth
     *
     * @param string $name
     * Required
     *
     * @param string $location
     * Required
     *
     * @param string $type
     * Required. Allowed values are 'Free' or 'Paid'
     *
     * @param string $price
     * Required when type is 'Paid'. Accepts only numeric values
     *
     * @param string $date
     * Required
     *
     * @param string $time
     * Required
     *
     * @param string $description
     * Required
     *
     * @param string $promo_image
     * Not required
     *
     *
     * @param string $twitter_hashtag
     * Not required
     *
     *
     * @param string $facebook_hashtag
     * Not required
     *
     *
     * @param string $youtube_live_stream_url
     * Not required
     *
     * @param string $download_link
     * Not required
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $event_data = $request->all();
            $event = new Event();

            if ($event->validate($event_data, $event->SCENARIO_CREATE)) {
                $event = Auth::user()->events()->create($event_data);

                return Response::success($event);

            } else {
                return Response::error($event->errors());
            }

        } catch (\Exception $e) {
            dd($e->getMessage());
            return Response::internalServerError("Internal Error");
        }
    }

    /**
     * Display the specified resource. Requires Auth
     *
     * @param  string  $token
     *
     * @param  int  $event
     * ID if the event
     * @return \Illuminate\Http\Response
     */
    public function show($event)
    {
        $event = Event::where('id', $event)->where('user_id', Auth::User()->id)->first();
        if($event) {
            return Response::success($event);
        }
        return Response::notFound();
    }

    /**
     * Stores an image to the application's image repository
     * Returns the image url
     *
     * @param image $promo_image
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeImage(Request $request) {
        if ($request->hasFile('promo_image') && $request->file('promo_image')->isValid()) {

            $filename = str_random(20) . "." . $request->file('promo_image')->getClientOriginalExtension();

            if (!$url = Storage::put('events/' . $filename, file_get_contents($request->file('promo_image')))) {
                return Response::error("File Upload Failed", []);
            }
            return Response::success(["url" => Storage::url('events/' . $filename)]);
        } else {

            return Response::error("No file / invalid file sent");
        }
    }

    /**
     * NOT IMPLEMENTED
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update an Event resource.  Requires Auth
     *
     * @param string $event
     * Required. Event id
     *
     * @param string $name
     * Required
     *
     * @param string $location
     * Required
     *
     * @param string $type
     * Required. Allowed values are 'Free' or 'Paid'
     *
     * @param string $price
     * Required when type is 'Paid'. Accepts only numeric values
     *
     * @param string $date
     * Required
     *
     * @param string $time
     * Required
     *
     * @param string $description
     * Required
     *
     * @param string $promo_image
     * Not required
     *
     *
     * @param string $twitter_hashtag
     * Not required
     *
     *
     * @param string $facebook_hashtag
     * Not required
     *
     *
     * @param string $youtube_live_stream_url
     * Required
     *
     * @param string $download_link
     * Required
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $event)
    {
        $event = Event::find($event);

        if(Auth::User()->can('update', $event)) {

            try {
                $event_data = $request->all();


                if ($event->validate($event_data, $event->SCENARIO_UPDATE)) {
                    $event->update($event_data);

                    return Response::success($event);

                } else {
                    return Response::error($event->errors());
                }

            } catch (\Exception $e) {
                dd($e->getMessage());
                return Response::internalServerError("Internal Error");
            }

        } else {
            return Response::unauthorized("Action Not Allowed");
        }

    }

    /**
     * Deletes an event. Requires Auth
     *
     * @param  int  $event
     * Required. Event id
     * @return \Illuminate\Http\Response
     */
    public function destroy($event)
    {
        try {
            $event =  Event::findorFail($event);

            if(Auth::User()->can('delete', $event)) {

                if($event->delete()) {
                    return Response::success($event);
                } else {
                    return Response::error("Event could not be deleted");
                }

            } else {
                return Response::unauthorized("Action Not Allowed");
            }
        } catch (ModelNotFoundException $e) {
            return Response::error("Event not found");
        }
    }
}
