<?php
/**
 * Created by PhpStorm.
 * User: mayowa
 * Date: 15/01/2017
 * Time: 5:46 AM
 */

namespace App\Http\Middleware;

use Closure;

class PreflightResponse
{
    /**
     * @param $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->getMethod() === 'OPTIONS') {
            return response('');
        }

        return $next($request);
    }
}