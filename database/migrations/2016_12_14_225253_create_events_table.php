<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->string('location');
            $table->enum('type', ['paid', 'free'])->default('free');
            $table->date('date');
            $table->time('time');
            $table->text('description');
            $table->text('promo_image_url')->nullable();
            $table->integer('views')->default(0);
            $table->string('twitter_hashtag')->nullable();
            $table->string('facebook_hashtag')->nullable();
            $table->string('youtube_live_stream_url')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
