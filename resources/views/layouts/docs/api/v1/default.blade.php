<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        @include('includes.docs.api.v1.head')
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-3" id="sidebar">
                    <div class="column-content">
                        <div class="search-header">
                            <img src="/assets/docs/api.v1/img/crystal-logo-nu.png" class="logo" alt="Logo" />
                            <input id="search" type="text" placeholder="Search">
                        </div>
                        <ul id="navigation">

                            <li><a href="#introduction">Introduction</a></li>

                            

                            <li>
                                <a href="#Closure">Closure</a>
                                <ul></ul>
                            </li>


                            <li>
                                <a href="#Login">Login</a>
                                <ul>
									<li><a href="#Login_login">login</a></li>
</ul>
                            </li>


                            <li>
                                <a href="#Register">Register</a>
                                <ul>
									<li><a href="#Register_register">register</a></li>
</ul>
                            </li>


                            <li>
                                <a href="#Event">Event</a>
                                <ul>
									<li><a href="#Event_index">index</a></li>

									<li><a href="#Event_show">show</a></li>

									<li><a href="#Event_like">like</a></li>
</ul>
                            </li>


                            <li>
                                <a href="#UserEvent">UserEvent</a>
                                <ul>
									<li><a href="#UserEvent_index">index</a></li>

									<li><a href="#UserEvent_create">create</a></li>

									<li><a href="#UserEvent_store">store</a></li>

									<li><a href="#UserEvent_show">show</a></li>

									<li><a href="#UserEvent_edit">edit</a></li>

									<li><a href="#UserEvent_update">update</a></li>

									<li><a href="#UserEvent_destroy">destroy</a></li>

									<li><a href="#UserEvent_update">update</a></li>

									<li><a href="#UserEvent_destroy">destroy</a></li>

									<li><a href="#UserEvent_storeImage">storeImage</a></li>
</ul>
                            </li>


                            <li>
                                <a href="#Users">Users</a>
                                <ul>
									<li><a href="#Users_index">index</a></li>

									<li><a href="#Users_create">create</a></li>

									<li><a href="#Users_store">store</a></li>

									<li><a href="#Users_show">show</a></li>

									<li><a href="#Users_edit">edit</a></li>

									<li><a href="#Users_update">update</a></li>

									<li><a href="#Users_destroy">destroy</a></li>

									<li><a href="#Users_enable">enable</a></li>

									<li><a href="#Users_disable">disable</a></li>

									<li><a href="#Users_update">update</a></li>

									<li><a href="#Users_destroy">destroy</a></li>
</ul>
                            </li>


                        </ul>
                    </div>
                </div>
                <div class="col-9" id="main-content">

                    <div class="column-content">

                        @include('includes.docs.api.v1.introduction')

                        <hr />

                                                

                                                <a href="#" class="waypoint" name="Closure"></a>
                        <h2>Closure</h2>
                        <p></p>

                                                

                                                <a href="#" class="waypoint" name="Login"></a>
                        <h2>Login</h2>
                        <p></p>

                        
                        <a href="#" class="waypoint" name="Login_login"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>login</h3></li>
                            <li>api/v1/login</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Validate User and return JWT token</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/login" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">email</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">User's email</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="email">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">password</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">User's password</div>
                                <div class="parameter-value">
                                    <input type="password" class="parameter-value-text" name="password">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>
                        

                                                <a href="#" class="waypoint" name="Register"></a>
                        <h2>Register</h2>
                        <p></p>

                        
                        <a href="#" class="waypoint" name="Register_register"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>register</h3></li>
                            <li>api/v1/register</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Handle a registration request for the application.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/register" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">name</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">User's name</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="name">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">email</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">User's email</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="email">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">password</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">User's password</div>
                                <div class="parameter-value">
                                    <input type="password" class="parameter-value-text" name="password">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">password_confirmation</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">User's password</div>
                                <div class="parameter-value">
                                    <input type="password" class="parameter-value-text" name="password_confirmation">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">role</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">User's role (admin or user)</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="role">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>
                        

                                                <a href="#" class="waypoint" name="Event"></a>
                        <h2>Event</h2>
                        <p></p>

                        
                        <a href="#" class="waypoint" name="Event_index"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>index</h3></li>
                            <li>api/v1/events</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Display a paginated list of events.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/events" type="GET">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">per-page</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Number of items returned at once</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="per-page">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Event_show"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>show</h3></li>
                            <li>api/v1/events/{event}</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Display the specified event.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/events/{event}" type="GET">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">event</div>
                                <div class="parameter-type">int</div>
                                <div class="parameter-desc">ID of the event</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="event">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Event_like"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>like</h3></li>
                            <li>api/v1/events/{event}/like</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Allow user like or unlike an event</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/events/{event}/like" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">event</div>
                                <div class="parameter-type">int</div>
                                <div class="parameter-desc">ID of the event</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="event">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">token</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc"></div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="token">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>
                        

                                                <a href="#" class="waypoint" name="UserEvent"></a>
                        <h2>UserEvent</h2>
                        <p></p>

                        
                        <a href="#" class="waypoint" name="UserEvent_index"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>index</h3></li>
                            <li>api/v1/user/events</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Display a listing of the resource. Requires Auth</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/user/events" type="GET">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">token</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc"></div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="token">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="UserEvent_create"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>create</h3></li>
                            <li>api/v1/user/events/create</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">NOT IMPLEMENTED.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/user/events/create" type="GET">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="UserEvent_store"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>store</h3></li>
                            <li>api/v1/user/events</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Store a newly created resource in storage. Requires Auth</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/user/events" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">name
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="name
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">location
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="location
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">type
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required. Allowed values are 'Free' or 'Paid'</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="type
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">price
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required when type is 'Paid'. Accepts only numeric values</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="price
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">date
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="date
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">time
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="time
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">description
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="description
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">promo_image
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Not required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="promo_image
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">twitter_hashtag
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Not required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="twitter_hashtag
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">facebook_hashtag
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Not required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="facebook_hashtag
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">youtube_live_stream_url
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="youtube_live_stream_url
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">download_link
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="download_link
">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="UserEvent_show"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>show</h3></li>
                            <li>api/v1/user/events/{event}</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Display the specified resource. Requires Auth</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/user/events/{event}" type="GET">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">token</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc"></div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="token">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">event
</div>
                                <div class="parameter-type">int</div>
                                <div class="parameter-desc">ID if the event</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="event
">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="UserEvent_edit"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>edit</h3></li>
                            <li>api/v1/user/events/{event}/edit</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">NOT IMPLEMENTED</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/user/events/{event}/edit" type="GET">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">id</div>
                                <div class="parameter-type">int</div>
                                <div class="parameter-desc"></div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="id">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="UserEvent_update"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>PUT</h2></li>
                            <li><h3>update</h3></li>
                            <li>api/v1/user/events/{event}</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Update an Event resource.  Requires Auth</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/user/events/{event}" type="PUT">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">event
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required. Event id</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="event
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">name
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="name
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">location
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="location
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">type
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required. Allowed values are 'Free' or 'Paid'</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="type
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">price
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required when type is 'Paid'. Accepts only numeric values</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="price
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">date
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="date
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">time
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="time
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">description
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="description
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">promo_image
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Not required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="promo_image
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">twitter_hashtag
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Not required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="twitter_hashtag
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">facebook_hashtag
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Not required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="facebook_hashtag
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">youtube_live_stream_url
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="youtube_live_stream_url
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">download_link
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="download_link
">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="PUT"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="UserEvent_destroy"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>DELETE</h2></li>
                            <li><h3>destroy</h3></li>
                            <li>api/v1/user/events/{event}</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Deletes an event. Requires Auth</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/user/events/{event}" type="DELETE">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">event
</div>
                                <div class="parameter-type">int</div>
                                <div class="parameter-desc">Required. Event id</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="event
">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="DELETE"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="UserEvent_update"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>update</h3></li>
                            <li>api/v1/user/events/{event}/update</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Update an Event resource.  Requires Auth</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/user/events/{event}/update" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">event
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required. Event id</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="event
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">name
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="name
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">location
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="location
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">type
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required. Allowed values are 'Free' or 'Paid'</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="type
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">price
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required when type is 'Paid'. Accepts only numeric values</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="price
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">date
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="date
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">time
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="time
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">description
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="description
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">promo_image
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Not required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="promo_image
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">twitter_hashtag
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Not required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="twitter_hashtag
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">facebook_hashtag
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Not required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="facebook_hashtag
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">youtube_live_stream_url
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="youtube_live_stream_url
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">download_link
</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="download_link
">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="UserEvent_destroy"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>destroy</h3></li>
                            <li>api/v1/user/events/{event}/delete</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Deletes an event. Requires Auth</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/user/events/{event}/delete" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">event
</div>
                                <div class="parameter-type">int</div>
                                <div class="parameter-desc">Required. Event id</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="event
">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="UserEvent_storeImage"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>storeImage</h3></li>
                            <li>api/v1/user/events/image</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Stores and image to the applications image repository
Returns the image url</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/user/events/image" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">image$promo_image
</div>
                                <div class="parameter-type">\image</div>
                                <div class="parameter-desc">Not required</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="image$promo_image
">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">Request$request</div>
                                <div class="parameter-type">\Request</div>
                                <div class="parameter-desc"></div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="Request$request">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>
                        

                                                <a href="#" class="waypoint" name="Users"></a>
                        <h2>Users</h2>
                        <p></p>

                        
                        <a href="#" class="waypoint" name="Users_index"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>index</h3></li>
                            <li>api/v1/admin/users</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Display a listing of the resource. Requires Auth</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/admin/users" type="GET">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">token</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc"></div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="token">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Users_create"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>create</h3></li>
                            <li>api/v1/admin/users/create</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Not Yet Implemented</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/admin/users/create" type="GET">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Users_store"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>store</h3></li>
                            <li>api/v1/admin/users</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Not Yet Implemented</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/admin/users" type="POST">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Users_show"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>show</h3></li>
                            <li>api/v1/admin/users/{user}</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Display the specified resource. Requires Auth</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/admin/users/{user}" type="GET">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">token</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc"></div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="token">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">id
</div>
                                <div class="parameter-type">int</div>
                                <div class="parameter-desc">ID if the user</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="id
">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Users_edit"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>edit</h3></li>
                            <li>api/v1/admin/users/{user}/edit</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Not yet implemented</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/admin/users/{user}/edit" type="GET">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Users_update"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>PUT</h2></li>
                            <li><h3>update</h3></li>
                            <li>api/v1/admin/users/{user}</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Not yet implemented</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/admin/users/{user}" type="PUT">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="PUT"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Users_destroy"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>DELETE</h2></li>
                            <li><h3>destroy</h3></li>
                            <li>api/v1/admin/users/{user}</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Not Yet implelemented</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/admin/users/{user}" type="DELETE">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="DELETE"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Users_enable"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>enable</h3></li>
                            <li>api/v1/admin/users/{id}/enable</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Enable User - Requires Auth</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/admin/users/{id}/enable" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">token</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc"></div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="token">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">id</div>
                                <div class="parameter-type"></div>
                                <div class="parameter-desc"></div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="id">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Users_disable"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>disable</h3></li>
                            <li>api/v1/admin/users/{id}/disable</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Disable User - Requires Auth</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/admin/users/{id}/disable" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">token</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc"></div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="token">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">id</div>
                                <div class="parameter-type"></div>
                                <div class="parameter-desc"></div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="id">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Users_update"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>update</h3></li>
                            <li>api/v1/admin/users/{user}/update</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Not yet implemented</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/admin/users/{user}/update" type="POST">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Users_destroy"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>destroy</h3></li>
                            <li>api/v1/admin/users/{user}/delete</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Not Yet implelemented</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/admin/users/{user}/delete" type="POST">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>


                    </div>
                </div>
            </div>
        </div>


    </body>
</html>
