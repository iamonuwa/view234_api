<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// CORS
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');

Route::get('/', function () {
    return response('Crystal Digest API');
});

/**
 * Handler for all v1 endpoints
 */
Route::group(['prefix' => 'v1'], function () {

    Route::get('/', function () {
        return response('Crystal Digest v1 API');
    });

    // Authentication Routes...
    $this->post('login', 'Auth\LoginController@login');
    $this->post('register', 'Auth\RegisterController@register');


    //Routes for events
    $this->get('events', 'EventController@index');
    $this->get('events/{event}', 'EventController@show');


    /**
     * Handler for endpoints that require authentication
     */
    Route::group(['middleware' => ['jwt.auth']], function () {

        $this->post('events/{event}/like', 'EventController@like');

        Route::group(['prefix' => 'user'], function () {

                Route::resource('events', 'User\UserEventController');

                /*
                 * These endpoints are implemented due to a CORS issue on the angular FE client
                 */
            $this->post('events/{event}/update', 'User\UserEventController@update');
            $this->post('events/{event}/delete', 'User\UserEventController@destroy');
            $this->post('events/image', 'User\UserEventController@storeImage');

        });

        /*
         * Handler for admin endpoints
         */
        Route::group(['prefix' => 'admin', 'middleware' => ['is:admin']], function () {

            Route::resource('users', 'Admin\UsersController');

            Route::group(['prefix' => 'users'], function () {

                $this->post('{id}/enable', 'Admin\UsersController@enable');
                $this->post('{id}/disable', 'Admin\UsersController@disable');

                /*
                 * These are implemented due to a CORS issue on the angular FE client
                 */
                $this->post('{user}/update', 'Admin\UsersController@update');
                $this->post('{user}/delete', 'Admin\UsersController@destroy');
            });

        });
    });

    if (App::environment('development'))
    {
        Route::get('docs', function(){
            return View::make('docs.api.v1.index');
        });
    }
});